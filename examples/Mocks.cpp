#include <iostream>
#include <rili/Test.hpp>
#include <rili/test/CallHandler.hpp>
#include <rili/test/Config.hpp>
#include <rili/test/EventHandler.hpp>
#include "Assert.hpp"

class MocksSuccess : public rili::test::TestBaseFixture {
 public:
    void after() override {
        rili::test::CallHandler::getInstance().tearDown();  // normally you not need this - it is called anyway by
        // runner after every test, however this is our self test, so we
        // would like to assert if any unexpected stuff dangling
        EXAMPLES_ASSERT(!rili::test::EventHandler::getInstance().currentTestFailed());
    }
};

class MocksFailure : public rili::test::TestBaseFixture {
 public:
    void after() override {
        rili::test::CallHandler::getInstance().tearDown();  // normally you not need this - it is called anyway by
        // runner after every test, however this is our self test, so we
        // would like to assert if any unexpected stuff dangling
        EXAMPLES_ASSERT(rili::test::EventHandler::getInstance().currentTestFailed());
    }
};

class Class {
 public:
    virtual void foo() {}
    virtual void foo() const {}
    virtual int bar() { return 5; }
    virtual int bar() const { return 10; }
    virtual int m1(int const& i1) const { return i1 * 0; }
    virtual int m2(int const& i1, int const& i2) const { return i1 * i2 * 0; }
    virtual int m3(int const& i1, int const& i2, int const& i3) const { return i1 * i2 * i3 * 0; }
    virtual int m4(int const& i1, int const& i2, int const& i3, int const& i4) const { return i1 * i2 * i3 * i4 * 0; }
    virtual int m5(int const& i1, int const& i2, int const& i3, int const& i4, int const& i5) const {
        return i1 * i2 * i3 * i4 * i5 * 0;
    }
    virtual int m6(int const& i1, int const& i2, int const& i3, int const& i4, int const& i5, int const& i6) const {
        return i1 * i2 * i3 * i4 * i5 * i6 * 0;
    }
    virtual int m7(int const& i1, int const& i2, int const& i3, int const& i4, int const& i5, int const& i6,
                   int const& i7) const {
        return i1 * i2 * i3 * i4 * i5 * i6 * i7 * 0;
    }
    virtual int m8(int const& i1, int const& i2, int const& i3, int const& i4, int const& i5, int const& i6,
                   int const& i7, int const& i8) const {
        return i1 * i2 * i3 * i4 * i5 * i6 * i7 * i8 * 0;
    }
    virtual int m9(int const& i1, int const& i2, int const& i3, int const& i4, int const& i5, int const& i6,
                   int const& i7, int const& i8, int const& i9) const {
        return i1 * i2 * i3 * i4 * i5 * i6 * i7 * i8 * i9 * 0;
    }
    virtual int m10(int const& i1, int const& i2, int const& i3, int const& i4, int const& i5, int const& i6,
                    int const& i7, int const& i8, int const& i9, int const& i10) const {
        return i1 * i2 * i3 * i4 * i5 * i6 * i7 * i8 * i9 * i10 * 0;
    }
    virtual ~Class() = default;
};

class Mock : public Class {
 public:
    Mock() {}
    virtual ~Mock() = default;

 public:
    MOCK0(void, foo)
    MOCK0(int, bar)
    MOCK0(void, foo, const)
    MOCK0(int, bar, const)
    MOCK1(int, m1, int const&, const)
    MOCK2(int, m2, int const&, int const&, const)
    MOCK3(int, m3, int const&, int const&, int const&, const)
    MOCK4(int, m4, int const&, int const&, int const&, int const&, const)
    MOCK5(int, m5, int const&, int const&, int const&, int const&, int const&, const)
    MOCK6(int, m6, int const&, int const&, int const&, int const&, int const&, int const&, const)
    MOCK7(int, m7, int const&, int const&, int const&, int const&, int const&, int const&, int const&, const)
    MOCK8(int, m8, int const&, int const&, int const&, int const&, int const&, int const&, int const&, int const&,
          const)
    MOCK9(int, m9, int const&, int const&, int const&, int const&, int const&, int const&, int const&, int const&,
          int const&, const)
    MOCK10(int, m10, int const&, int const&, int const&, int const&, int const&, int const&, int const&, int const&,
           int const&, int const&, const)
};

TEST_F(MocksSuccess, foo) {
    Mock instance;

    EXPECT_CALL(instance, foo, []() { std::cout << "foo was called" << std::endl; });
    instance.foo();
}

TEST_F(MocksSuccess, fooConst) {
    const Mock instance;

    EXPECT_CALL(instance, foo, []() { std::cout << "const foo was called" << std::endl; });
    instance.foo();
}

TEST_F(MocksSuccess, bar) {
    Mock instance;

    EXPECT_CALL(instance, bar, []() {
        std::cout << "bar was called" << std::endl;
        return 1;
    });
    EXPECT_EQ(1, instance.bar());
}

TEST_F(MocksSuccess, barConst) {
    const Mock instance;

    EXPECT_CALL(instance, bar, []() {
        std::cout << "const bar was called" << std::endl;
        return 2;
    });
    EXPECT_EQ(2, instance.bar());
}

#ifdef RILI_TEST_WITH_EXCEPTIONS
TEST_F(MocksFailure, unexpectedCall) {
    const Mock instance;
    EXPECT_EQ(0, instance.m4(1, 2, 3, 4));
}
#endif

TEST_F(MocksFailure, danglingExpectation) {
    const Mock instance;
    EXPECT_CALL(instance, bar, []() { return 5; }, "this is expected to be not called");
}

TEST_F(MocksSuccess, m1) {
    const Mock instance;

    EXPECT_CALL(instance, m1, [](int const& i) {
        EXPECT_EQ(1, i);
        std::cout << "const m1 was called" << std::endl;
        return i;
    });
    EXPECT_EQ(1, instance.m1(1));
}

TEST_F(MocksSuccess, m2) {
    const Mock instance;

    EXPECT_CALL(instance, m2, [](int const& i1, int const& i2) {
        EXPECT_EQ(1, i1);
        EXPECT_EQ(2, i2);
        std::cout << "const m2 was called" << std::endl;
        return i1 * i2;
    });
    EXPECT_EQ(2, instance.m2(1, 2));
}

TEST_F(MocksSuccess, m3) {
    const Mock instance;

    EXPECT_CALL(instance, m3, [](int const& i1, int const& i2, int const& i3) {
        EXPECT_EQ(1, i1);
        EXPECT_EQ(2, i2);
        EXPECT_EQ(3, i3);
        std::cout << "const m3 was called" << std::endl;
        return i1 * i2 * i3;
    });
    EXPECT_EQ(6, instance.m3(1, 2, 3));
}

TEST_F(MocksSuccess, m4) {
    const Mock instance;

    EXPECT_CALL(instance, m4, [](int const& i1, int const& i2, int const& i3, int const& i4) {
        EXPECT_EQ(1, i1);
        EXPECT_EQ(2, i2);
        EXPECT_EQ(3, i3);
        EXPECT_EQ(4, i4);
        std::cout << "const m4 was called" << std::endl;
        return i1 * i2 * i3 * i4;
    });
    EXPECT_EQ(24, instance.m4(1, 2, 3, 4));
}

TEST_F(MocksSuccess, m5) {
    const Mock instance;

    EXPECT_CALL(instance, m5, [](int const& i1, int const& i2, int const& i3, int const& i4, int const& i5) {
        EXPECT_EQ(1, i1);
        EXPECT_EQ(2, i2);
        EXPECT_EQ(3, i3);
        EXPECT_EQ(4, i4);
        EXPECT_EQ(5, i5);
        std::cout << "const m5 was called" << std::endl;
        return i1 * i2 * i3 * i4 * i5;
    });
    EXPECT_EQ(120, instance.m5(1, 2, 3, 4, 5));
}

TEST_F(MocksSuccess, m6) {
    const Mock instance;

    EXPECT_CALL(instance, m6,
                [](int const& i1, int const& i2, int const& i3, int const& i4, int const& i5, int const& i6) {
                    EXPECT_EQ(1, i1);
                    EXPECT_EQ(2, i2);
                    EXPECT_EQ(3, i3);
                    EXPECT_EQ(4, i4);
                    EXPECT_EQ(5, i5);
                    EXPECT_EQ(6, i6);
                    std::cout << "const m6 was called" << std::endl;
                    return i1 * i2 * i3 * i4 * i5 * i6;
                });
    EXPECT_EQ(720, instance.m6(1, 2, 3, 4, 5, 6));
}

TEST_F(MocksSuccess, m7) {
    const Mock instance;

    EXPECT_CALL(instance, m7, [](int const& i1, int const& i2, int const& i3, int const& i4, int const& i5,
                                 int const& i6, int const& i7) {
        EXPECT_EQ(1, i1);
        EXPECT_EQ(2, i2);
        EXPECT_EQ(3, i3);
        EXPECT_EQ(4, i4);
        EXPECT_EQ(5, i5);
        EXPECT_EQ(6, i6);
        EXPECT_EQ(7, i7);
        std::cout << "const m7 was called" << std::endl;
        return i1 * i2 * i3 * i4 * i5 * i6 * i7;
    });
    EXPECT_EQ(5040, instance.m7(1, 2, 3, 4, 5, 6, 7));
}

TEST_F(MocksSuccess, m8) {
    const Mock instance;

    EXPECT_CALL(instance, m8, [](int const& i1, int const& i2, int const& i3, int const& i4, int const& i5,
                                 int const& i6, int const& i7, int const& i8) {
        EXPECT_EQ(1, i1);
        EXPECT_EQ(2, i2);
        EXPECT_EQ(3, i3);
        EXPECT_EQ(4, i4);
        EXPECT_EQ(5, i5);
        EXPECT_EQ(6, i6);
        EXPECT_EQ(7, i7);
        EXPECT_EQ(8, i8);
        std::cout << "const m8 was called" << std::endl;
        return i1 * i2 * i3 * i4 * i5 * i6 * i7 * i8;
    });
    EXPECT_EQ(40320, instance.m8(1, 2, 3, 4, 5, 6, 7, 8));
}

TEST_F(MocksSuccess, m9) {
    const Mock instance;

    EXPECT_CALL(instance, m9, [](int const& i1, int const& i2, int const& i3, int const& i4, int const& i5,
                                 int const& i6, int const& i7, int const& i8, int const& i9) {
        EXPECT_EQ(1, i1);
        EXPECT_EQ(2, i2);
        EXPECT_EQ(3, i3);
        EXPECT_EQ(4, i4);
        EXPECT_EQ(5, i5);
        EXPECT_EQ(6, i6);
        EXPECT_EQ(7, i7);
        EXPECT_EQ(8, i8);
        EXPECT_EQ(9, i9);
        std::cout << "const m9 was called" << std::endl;
        return i1 * i2 * i3 * i4 * i5 * i6 * i7 * i8 * i9;
    });
    EXPECT_EQ(362880, instance.m9(1, 2, 3, 4, 5, 6, 7, 8, 9));
}

TEST_F(MocksSuccess, m10) {
    const Mock instance;

    EXPECT_CALL(instance, m10, [](int const& i1, int const& i2, int const& i3, int const& i4, int const& i5,
                                  int const& i6, int const& i7, int const& i8, int const& i9, int const& i10) {
        EXPECT_EQ(1, i1);
        EXPECT_EQ(2, i2);
        EXPECT_EQ(3, i3);
        EXPECT_EQ(4, i4);
        EXPECT_EQ(5, i5);
        EXPECT_EQ(6, i6);
        EXPECT_EQ(7, i7);
        EXPECT_EQ(8, i8);
        EXPECT_EQ(9, i9);
        EXPECT_EQ(10, i10);
        std::cout << "const m9 was called" << std::endl;
        return i1 * i2 * i3 * i4 * i5 * i6 * i7 * i8 * i9 * i10;
    });
    EXPECT_EQ(3628800, instance.m10(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
}
