#pragma once
#include <cassert>
#include <exception>

#ifdef NDEBUG
inline void examples_assert(bool exprResult) {
    if (!exprResult) {
        std::terminate();
    }
}
#define EXAMPLES_ASSERT(X) examples_assert(X)
#else
#define EXAMPLES_ASSERT(X) assert(X)
#endif
