#include <regex>
#include <rili/Test.hpp>
#include <string>

int main(int /*argc*/, char** /*argv*/) {
    rili::test::runner::run([](std::string const& /*fixture*/, std::string const& scenario) {
        if (std::regex_match(scenario, std::regex("(SKIP_)(.*)"))) {
            return rili::test::runner::FilteringResult::Skip;
        } else if (std::regex_match(scenario, std::regex("(DISABLE_)(.*)"))) {
            return rili::test::runner::FilteringResult::Disable;
        } else {
            return rili::test::runner::FilteringResult::Run;
        }
    });
    return 0;
}
