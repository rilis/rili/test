#include <atomic>
#include <cstdint>
#include <rili/Test.hpp>
#include <rili/test/Config.hpp>
#include <type_traits>
#include <utility>
#include "Assert.hpp"

TYPED_TEST(TypedTest, atomic_increment_operator) {
    TypeParam atomic = {};
    auto value = atomic.load();
    value++;
    atomic++;
    EXPECT_EQ(value, atomic.load());
}
PARAMETERIZE_TYPED_TEST(TypedTest, atomic_increment_operator, std::atomic<uint8_t>, std::atomic<uint16_t>,
                        std::atomic<uint32_t>, std::atomic<uint64_t>, std::atomic<int8_t>, std::atomic<int16_t>,
                        std::atomic<int32_t>, std::atomic<int64_t>)

template <typename T>
using uni_pair = std::pair<T, T>;

TYPED_TEST(TypedTest, uni_pair_equatity) {
    bool equal = std::is_same<typename TypeParam::first_type, typename TypeParam::second_type>::value;
    EXPECT_TRUE(equal);
}
PARAMETERIZE_TYPED_TEST(TypedTest, uni_pair_equatity, uni_pair<int>, uni_pair<double>, uni_pair<std::atomic<double>>)

TYPED_TEST(TypedTest, default_construction) {
    TypeParam param = {};
    (void)param;
}
PARAMETERIZE_TYPED_TEST(TypedTest, default_construction, int, float, double, int64_t, unsigned, std::pair<int, int>,
                        std::pair<int, int32_t>, std::pair<std::atomic<double>, std::atomic<int>>,
                        std::pair<std::atomic<double>, std::atomic<int32_t>>,
                        std::pair<std::atomic<int>, std::atomic<int32_t>>,
                        std::pair<std::atomic<double>, std::atomic<double>>,
                        std::pair<std::atomic<float>, std::atomic<int32_t>>)

template <typename FixtureType>
class TypedFixture : public rili::test::TestBaseFixture {
 public:
    TypedFixture() : m_FixtureMember() {}
    FixtureType m_FixtureMember;
};

TYPED_TEST_F(TypedFixture, basic) {
    TypeParam local;
    (void)local;
    bool equal = std::is_same<decltype(local), decltype(this->m_FixtureMember)>::value;
    EXPECT_TRUE(equal);
}
PARAMETERIZE_TYPED_TEST(TypedFixture, basic, int, double, std::pair<std::atomic<double>, std::atomic<int32_t>>,
                        std::atomic<int32_t>)

TEST(TypedTest, getTypeNames_TypesWithSpace) {
#define GET_TYPE_NAMES(...) ::rili::test::getTypeNames<__VA_ARGS__>(#__VA_ARGS__)

    typedef int MyInt;
    // clang-format off
    auto typenames =
        GET_TYPE_NAMES(char, unsigned char, short int,                          // NOLINT (runtime/int)
                       unsigned short int,   int, unsigned int, long, unsigned  // NOLINT (runtime/int)
                       long int,                                                // NOLINT (runtime/int)
                       long long int, unsigned long   long int, MyInt);         // NOLINT (runtime/int)
// clang-format on
#undef GET_TYPE_NAMES
    EXAMPLES_ASSERT(typenames.size() == 11u);
    EXAMPLES_ASSERT(typenames[0] == "char");
    EXAMPLES_ASSERT(typenames[1] == "unsigned char");
    EXAMPLES_ASSERT(typenames[2] == "short int");
    EXAMPLES_ASSERT(typenames[3] == "unsigned short int");
    EXAMPLES_ASSERT(typenames[4] == "int");
    EXAMPLES_ASSERT(typenames[5] == "unsigned int");
    EXAMPLES_ASSERT(typenames[6] == "long");
    EXAMPLES_ASSERT(typenames[7] == "unsigned long int");
    EXAMPLES_ASSERT(typenames[8] == "long long int");
    EXAMPLES_ASSERT(typenames[9] == "unsigned long long int");
    EXAMPLES_ASSERT(typenames[10] == "MyInt");
}

TEST(TypedTest, getTypeNames_TypesTemplateParameters) {
#define GET_TYPE_NAMES(...) ::rili::test::getTypeNames<__VA_ARGS__>(#__VA_ARGS__)

    // clang-format off
    auto typenames =
        GET_TYPE_NAMES(std::pair<  int, int  >,
                       std::pair<int  ,
                       int32_t>, std::pair<     std::atomic<   /*some comment*/  double>, std::atomic<int>>,
                       std::pair<std::atomic<
                       double>,std::atomic<int32_t>>,  // NOLINT(whitespace/comma)
                       std::pair<std::atomic<int>, std::atomic<int32_t
                       >>,
                       std::pair<    std::atomic  <double
                       >, std::atomic<double>>,
                       std::pair<std::atomic<float>, std::
                       atomic<int32_t>>);
// clang-format on
#undef GET_TYPE_NAMES
    EXAMPLES_ASSERT(typenames.size() == 7u);
    EXAMPLES_ASSERT(typenames[0] == "std::pair<int, int>");
    EXAMPLES_ASSERT(typenames[1] == "std::pair<int, int32_t>");
    EXAMPLES_ASSERT(typenames[2] == "std::pair<std::atomic<double>, std::atomic<int>>");
    EXAMPLES_ASSERT(typenames[3] == "std::pair<std::atomic<double>, std::atomic<int32_t>>");
    EXAMPLES_ASSERT(typenames[4] == "std::pair<std::atomic<int>, std::atomic<int32_t>>");
    EXAMPLES_ASSERT(typenames[5] == "std::pair<std::atomic<double>, std::atomic<double>>");
    EXAMPLES_ASSERT(typenames[6] == "std::pair<std::atomic<float>, std::atomic<int32_t>>");
}
